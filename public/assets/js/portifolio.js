$(document).ready(function($) {

	var controleNav = false;
	$(document).on('scroll', function(e) {

		var scrollTop = $(document).scrollTop();

		if(scrollTop > 30){
			if(controleNav == false){
				$('.navbar').fadeIn("slow").removeClass('d-none');				
				controleNav = true;
				
			}

		}else{
			if(controleNav == true){
				$('.navbar').addClass('d-none');						
				controleNav = false;
			}
		}


	});


});